package middleware

import (
	"go-template/helper"
	"go-template/model/web"
	"net/http"
)

type AuthMiddleware struct {
	// meneruskan ke handle selanjutnya
	Handler http.Handler
}

func NewAuthMiddleware(handler http.Handler) *AuthMiddleware {
	return &AuthMiddleware{
		Handler:handler,
	}
}

func (middleware *AuthMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if "Rahasia" == request.Header.Get("X-API-KEY"){
		middleware.Handler.ServeHTTP(writer,request)
	}else{
		writer.Header().Set("Content-Type","applicaton/json")
		writer.WriteHeader(http.StatusUnauthorized)
		webResponse := web.WebResponse{
			Code: http.StatusUnauthorized,
			Status: "UNAUTHORIZED",
		}
		helper.WriteToResponseBody(writer,webResponse)
	}
}

